# Inundator Docker Image
A docker image that has [Inundator](http://inundator.sourceforge.net) installed on Ubuntu 12.04.  

## Building the Image
Build the docker image with the following command:

    $ docker build -t schwartz1375/inundator:latest -f ./Dockerfile .

## How to Launch the inundator  Docker Container
The most basic form is run the container and to start the inundator from a bash prompt after a proxy has been setup

        $ docker run -it --name=inundator schwartz1375/inundator

Once inside the container we can do the following to setup a SOCKS proxy:

        root@<Container ID>:~# ssh -D 9050 -f -q -N user@<proxy IP or FQN>

-D 9050 : This does the dynamic stuff and makes it behave as a SOCKS server.

-f : This will fork the process into the background after you type your password.

-q : Quiet mode. Since this is just a tunnel we can make it quiet.

-N : Tells it no commands will be sent. (the -f will complain if we don’t specify this)

Next to start Inundator type:

        root@<Container ID>:~# inundator -r /opt/community-rules -p localhost:9050 <Target>

Note a target can be an IP address or FQN, a IP range (e.g. 192.168.0.10-192.168.0.25) or an entire subnet in CIDR format (e.g. 192.168.0.0/16).
